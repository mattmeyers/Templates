import os
from flask import Flask, render_template, abort, request
from functools import wraps

# Authorization decorator. Protect endpoints by adding @requires_auth after the
# route decorator. This will require the request to pass a valid API key through
# the Authorization header.
def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        api_key = None
        if "Authorization" in request.headers:
            api_key = request.headers["Authorization"]
        if not api_key or api_key != os.getenv('API_KEY'):
            abort(401)
        return f(*args, **kwargs)
    return decorated

# Application factory. This function runs when app is initialized.  Endpoints and 
# more complicated authorization methods can be initialized here.
def create_app():
    app = Flask(__name__)

    @app.route('/')
    def index():
        return render_template('layout.html')

    return app
