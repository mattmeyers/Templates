import os
import sys
import logging

logging.basicConfig(stream=sys.stderr)

# Add the current directory to the python path.
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from app import create_app
application = create_app()
