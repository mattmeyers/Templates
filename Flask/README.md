# Flask API Project Template

This is a template for a Flask REST API.  Pipenv is used for controlling packages in a virtual environment and flask-cli is used to run the development server.  This template provides support for front-end development using the Jinja2 template engine.

## Requirements

* Python3
* Pipenv

## Setup

1. Rename the `app` directory to your `$project_name`.
1. Set `FLASK_APP` environment variable in `.env-template` to `$project_name/$project_name:create_app()` and rename file to `.env`.
1. If using token authorization, set `$API_KEY` in `.env`.  Use `@auth_required` decorator after `@app.route` decorator.
1. Add `$project_name` in `setup.py`.
1. Run `pipenv install` from root of project.
1. Install any additional packages by running `pipenv install <package_name>`.
1. Start development server with `pipenv run flask run`.

## Running the API

This project uses flask-cli to run.  So long as `FLASK_APP` is set, run `pipenv run flask run` from anywhere in the project to begin the development server.  This server should detect changes in the code and restart when necessary.  

When running on a production server, apache2 interfaces with the python code using a WSGI Daemon.  In the `site.conf` file, add:

```
WSGIDaemonProcess <WSGIProcessGroup name> python-home=/path/to/virtual/environment
WSGIScriptAlias / /path/to/wsgi.py

<Directory /path/to/document/root>
        WSGIProcessGroup <WSGIProcessGroup name>
        WSGIApplicationGroup %{GLOBAL}
        Require all granted
</Directory>
```

with a WSGIProcessGroup name and appropriate paths filled in.  It is sufficient to `touch` the `wsgi.py` file when pushing new code in order for apache to register a change and restart the Daemon process.
