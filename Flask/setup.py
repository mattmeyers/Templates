from setuptools import setup

setup(
    name='<app_name>',
    packages=['<app_name>'],
    include_package_data=True,
    install_requires=[
      'flask',
    ],
)
